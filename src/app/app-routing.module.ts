/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoggedGuardService } from './services/logged-guard.service';
import { ProfileGuardService } from './services/profile-guard.service';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: './home/home.module#HomeModule',
    canActivate: [ProfileGuardService]
  },
  {
    path: 'login',
    loadChildren: './login/login.module#LoginModule'
  },
  {
    path: 'profile',
    loadChildren: './user/user.module#UserModule',
    canActivate: [LoggedGuardService]
  },
  {
    path: 'chat',
    loadChildren: './chat/chat.module#ChatModule',
    canActivate: [LoggedGuardService, ProfileGuardService]
  },
  {
    path: 'places',
    loadChildren: './places/places.module#PlacesModule',
    canActivate: [LoggedGuardService, ProfileGuardService]
  },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
