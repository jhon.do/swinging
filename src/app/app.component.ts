/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'sg-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'Swinging';
  user: firebase.User;

  constructor(
    private authService: AngularFireAuth,
    private router: Router
  ) { }

  private subscription: Subscription;
  ngOnInit() {
    this.subscription = this.authService.user.subscribe(user => {
      this.user = user;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  logout(): void {
    this.authService.auth.signOut();
    delete this.user;
    this.router.navigate(['login']);
  }
}
