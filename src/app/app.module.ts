/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import {
  MatToolbarModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatMenuModule
} from '@angular/material';
import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import { bdd } from '../environments/bdd';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(bdd.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    MatToolbarModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatMenuModule,
    AgmCoreModule.forRoot({
      apiKey: bdd.geo,
      libraries: ['places']
    })
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'fr-CH' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
