import { CanActivate } from '@angular/router';
/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChatGuardService } from './services/chat-guard.service';
import { ChatComponent } from './chat/chat.component';
import { MessageListComponent } from './message-list/message-list.component';

const routes: Routes = [
  {
    path: '',
    component: ChatComponent,
    children: [
      {
        path: ':to',
        component: MessageListComponent,
        canActivate: [ChatGuardService]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChatRoutingModule { }
