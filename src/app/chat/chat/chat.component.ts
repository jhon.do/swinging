/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { Component } from '@angular/core';

@Component({
  selector: 'sg-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent {

  constructor() { }
}
