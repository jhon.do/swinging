/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { Subscription } from 'rxjs';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { Conversation } from '../model/conversation';
import { Profil } from '../../user/model/profil';

@Component({
  selector: 'sg-conversation-list',
  templateUrl: './conversation-list.component.html',
  styleUrls: ['./conversation-list.component.css']
})
export class ConversationListComponent implements OnInit, OnDestroy {
  conversations: string[] = [];
  loading = true;

  private userSubscription: Subscription;
  private conversationSubscription: Subscription;

  constructor(private authService: AngularFireAuth,
    private db: AngularFirestore) { }

  ngOnInit() {
    this.userSubscription = this.authService.user.subscribe(user => {
      if (!user) {
        this.loading = false;
        return;
      }
      this.conversationSubscription = this.db.collection<Conversation>('conversations',
        ref => ref.where(`attendees.${user.uid}`, '==', true))
        .valueChanges()
        .subscribe(conversations => {
          const ids = conversations
            .sort((a, b) => {
              if (a.timestamp === b.attendees) {
                return 0;
              }
              if (a.timestamp < b.attendees) {
                return -1;
              }
              return 1;
            })
            .map(c => {
              for (const key in c.attendees) {
                if (c.attendees.hasOwnProperty(key)) {
                  if (key !== user.uid) {
                    return key;
                  }
                }
              }
            });

          for (const id of ids) {
            this.db.collection<Profil>('users')
            .doc<Profil>(id)
            .valueChanges()
            .subscribe(p => {
              const index = this.conversations.findIndex(c => c === p.displayName);
              if (index > -1) {
                this.conversations.splice(index, 1);
              }
              this.conversations.unshift(p.displayName);
            });
          }
          this.loading = false;
        });
    });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
    if (this.conversationSubscription) {
      this.conversationSubscription.unsubscribe();
    }
  }

}
