/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { firestore } from 'firebase/app';

import { Profil } from '../../user/model/profil';
import { Conversation } from '../model/conversation';
import { Message } from '../model/message';

@Component({
  selector: 'sg-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.css']
})
export class MessageListComponent implements OnInit, OnDestroy {
  to: string;
  user: firebase.User;
  error: string;
  messages: Message[];
  form: FormGroup;
  loading = true;

  private userSubscription: Subscription;
  private messageSubscription: Subscription;
  private usersSubscription: Subscription;
  private conversationSubscription: Subscription;
  private convId: string;
  private attendeeId: string;

  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private authService: AngularFireAuth,
    private db: AngularFirestore) { }

  ngOnInit() {
    this.form = this.fb.group({
      message: ['', Validators.required]
    });
    this.route.paramMap.subscribe((params: ParamMap) => {
        this.to = params.get('to');
        this.loading = true;
        this.unsubscribe(this.userSubscription);
        this.userSubscription = this.authService.user.subscribe(user => {
          if (!user) {
            this.loading = false;
            return;
          }
          this.user = user;
          this.unsubscribe(this.usersSubscription);
          this.usersSubscription = this.db.collection<Profil>('users', ref => ref.where('displayName', '==', this.to))
            .stateChanges()
            .subscribe(userActions => {
              if (userActions.length === 0) {
                this.error = 'Cet utilisateur n\'existes pas.';
                this.loading = false;
              } else {
                this.attendeeId = userActions[0].payload.doc.id;
                this.unsubscribe(this.conversationSubscription);
                this.conversationSubscription = this.db.collection<Conversation>('conversations',
                  ref => ref.where(`attendees.${this.attendeeId}`, '==', true)
                    .where(`attendees.${this.user.uid}`, '==', true))
                  .stateChanges()
                  .subscribe(actions => {
                    if (actions.length > 0) {
                      const doc = actions[0].payload.doc;
                      this.convId = doc.id;
                      this.unsubscribe(this.messageSubscription);
                      this.messageSubscription = this.db.collection<Message>('messages',
                        ref => ref.where('convId', '==', this.convId)
                          .orderBy('timestamp', 'desc')
                          .limit(50))
                        .valueChanges()
                        .subscribe(messages => {
                          this.messages = messages.map((m, i, a) => {
                            if (m.timestamp) {
                              m.date = new Date(m.timestamp.seconds * 1000);
                            }
                            if (i > 0) {
                              let index = i - 1;
                              for (index; index > 0; index--) {
                                if (a[index].fromId) {
                                  break;
                                }
                              }
                              if (a[index].fromId === m.fromId) {
                                delete m.fromId;
                              }
                            }
                            return m;
                          });
                          this.loading = false;
                      });
                    } else {
                      this.loading = false;
                    }
                  });
              }
            });
        });
    });
  }

  ngOnDestroy() {
    this.unsubscribe(this.userSubscription);
    this.unsubscribe(this.usersSubscription);
    this.unsubscribe(this.conversationSubscription);
    this.unsubscribe(this.messageSubscription);
  }

  send(): void {
    const control = this.form.get('message');
    if (this.convId) {
      const subscription = this.db.collection<Conversation>('conversations')
        .doc<Conversation>(this.convId)
        .valueChanges()
        .subscribe(c => {
          subscription.unsubscribe();
          this.db.collection<Conversation>('conversations')
            .doc(this.convId)
            .update({timestamp: firestore.FieldValue.serverTimestamp()})
            .then(() => this.addMessage(control))
            .catch(() => this.error = 'Oups, il y a eu un problème.');
        });
    } else {
      const attendees: Map<string, boolean> = new Map<string, boolean>();
      attendees[this.user.uid] = true;
      attendees[this.attendeeId] = true;
      this.db.collection<Conversation>('conversations')
        .add({
          attendees: attendees,
          timestamp: firestore.FieldValue.serverTimestamp()
        })
        .then(c => {
          this.convId = c.id;
          this.addMessage(control);
        })
        .catch(() => this.error = 'Oups, il y a eu un problème.');
    }
  }

  private addMessage(control) {
    this.db.collection<Message>('messages')
      .add({
        convId: this.convId,
        fromId: this.user.uid,
        text: control.value,
        timestamp: firestore.FieldValue.serverTimestamp()
      })
      .then(() => control.reset())
      .catch(reaseon => this.error = 'Oups, il y a eu un problème.');
  }

  getTime(date: Date): string {
    if (!date) {
      return;
    }
    const now = new Date();
    if (now.getFullYear() === date.getFullYear()
      && now.getMonth() === date.getMonth()
      && now.getDate() === date.getDate()) {
        return date.toLocaleTimeString();
      }

      return `${date.toLocaleDateString()} ${this.pad(date.getHours())}:${this.pad(date.getMinutes())}`;
  }

  private pad(value: number): string {
    return value < 10 ? '0' + value : value.toString();
  }

  private unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
    }
  }
}
