export interface Conversation {
  attendees: Map<string, boolean>;
  timestamp: any;
}
