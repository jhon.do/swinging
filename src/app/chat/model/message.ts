export interface Message {
  convId: string;
  fromId: string;
  text: string;
  timestamp: any;
  date?: Date;
}
