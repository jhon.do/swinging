/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChatGuardService implements CanActivate {

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.authService.user.pipe(map(user => {
      const to = route.params.to;
      if (!user.displayName && to !== 'cplbimixte') {
        this.router.navigate(['profile']);
        return false;
      }
      return user.displayName !== to;
    }));
  }

  constructor(private authService: AngularFireAuth, private router: Router) { }
}
