/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { FormsModule } from '@angular/forms';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home/home.component';
import { bdd } from '../../environments/bdd';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    FormsModule,
    AgmCoreModule
  ],
  providers: [GoogleMapsAPIWrapper],
  declarations: [HomeComponent]
})
export class HomeModule { }
