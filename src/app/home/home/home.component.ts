/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
/// <reference types="googlemaps" />
import { Component, ViewChild, ElementRef } from '@angular/core';
import { LatLngLiteral } from '@agm/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { GeoFirestore } from 'geofirestore';
import { Router } from '@angular/router';


import { Place } from './../../places/model/place';
@Component({
  selector: 'sg-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  @ViewChild('autocomplete')
  inputRef: ElementRef;
  map: google.maps.Map;

  private markers: google.maps.Marker[] = [];
  private geoFirestore: GeoFirestore;
  private query: any;
  private collection: AngularFirestoreCollection<Place>;
  private pending = false;

  constructor(private db: AngularFirestore,
    private router: Router) { }


  mapReady(m: google.maps.Map): void {
      this.map = m;
      const input = <HTMLInputElement>this.inputRef.nativeElement;
      const autocomplete = new google.maps.places.Autocomplete(input);
      autocomplete.addListener('place_changed', () => {
        const place = autocomplete.getPlace();
        if (place && place.geometry && place.geometry.location) {
          const location = place.geometry.location;
          this.map.setCenter(location);
        }
      });
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(position => {
          const coords = position.coords;
          this.map.setCenter(new google.maps.LatLng(coords.latitude, coords.longitude));
        });
      }
  }

  centerChange(_: LatLngLiteral): void {
    if (this.pending) {
      return;
    }

    this.pending = true;
    setTimeout(() => {
      const bounds = this.map.getBounds();
      const center = this.map.getCenter();
      if (!bounds) {
        return;
      }
      const coords = [center.lat(), center.lng()];
      const northEast = bounds.getNorthEast();
      const raduis = GeoFirestore.distance(coords, [ northEast.lat(), northEast.lng()]);

      if (!this.geoFirestore) {
        this.collection = this.db.collection<Place>('places');
        this.geoFirestore = new GeoFirestore(this.collection.ref);
        this.query = this.geoFirestore.query({
          center: coords,
          radius: raduis
        });

        this.query.on('key_entered', key => {
          this.collection.ref.get(key)
            .then(value => {
              if (!value) {
                return;
              }
              value.forEach(doc => {
                if (!doc) {
                  return;
                }
                const place = doc.data();
                const marker = new google.maps.Marker({
                  position: new google.maps.LatLng(place.l[0], place.l[1]),
                  map: this.map,
                  title: place.d.name
                });
                marker.addListener('click', () => {
                  const info = new google.maps.InfoWindow({
                    content: `<div class="infoWindows"><a href="places/${doc.id}">${place.d.name}</a></div>`
                  });
                  info.open(this.map, marker);
                });
                this.markers.push(marker);
              });
            });
        });
        this.query.on('key_exited', (key) => {
          const index = this.markers.findIndex(m => m.getTitle() === key);
          if (index > -1) {
            const marker = this.markers[index];
            marker.setMap(null);
            this.markers.splice(index, 1);
          }
        });
      } else {
        this.query.updateCriteria({
          center: coords,
          radius: raduis
        });
      }
      this.pending = false;
    }, 100);
  }

  navigate(to: string): void {
    this.router.navigate([to]);
  }
}
