/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { AngularFireAuth } from 'angularfire2/auth';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sg-email-not-verified',
  templateUrl: './email-not-verified.component.html',
  styleUrls: ['./email-not-verified.component.css']
})
export class EmailNotVerifiedComponent implements OnInit {
  validationMessage: string;
  registerError: any;

  constructor(private authService: AngularFireAuth) { }

  ngOnInit() {
  }

  sendEmail(): void {
    this.authService.auth.currentUser
      .sendEmailVerification()
      .then(() => this.validationMessage = 'Un mail de confirmation t\'a été envoyé. Vérifie ta boite mail et clique sur le lien.')
      .catch(reason => this.registerError = reason.message);
  }
}
