/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ValidationComponent } from './validation/validation.component';
import { EmailNotVerifiedComponent } from './email-not-verified/email-not-verified.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'validation',
    component: ValidationComponent
  },
  {
    path: 'email-not-verified',
    component: EmailNotVerifiedComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
