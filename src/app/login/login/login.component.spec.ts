/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: AngularFireAuth;
  let router: jasmine.SpyObj<Router>;

  beforeEach(async(() => {
    const auth = {
      auth: {
        currentUser: undefined
      }
    } as AngularFireAuth;
    const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [ ReactiveFormsModule ],
      providers: [
        { provide: AngularFireAuth, useValue: auth },
        { provide: Router, useValue: routerSpy }
      ]
    })
    .compileComponents();
    router = TestBed.get(Router);
    authService = TestBed.get(AngularFireAuth);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
