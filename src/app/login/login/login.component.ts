/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'sg-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  loginError: string;
  showPassworForgotten = false;
  emailSent: string;

  constructor(private fb: FormBuilder,
    private authService: AngularFireAuth,
    private router: Router) { }

  ngOnInit() {
    this.createForm();
  }

  login(): void {
    const controls = this.form.controls;
    this.authService.auth.signInWithEmailAndPassword(controls['email'].value, controls['password'].value)
      .then(creds => {
        if (creds.user.emailVerified) {
          this.router.navigate(['home']);
          return;
        }
        this.router.navigate(['login/email-not-verified']);
      })
      .catch(reason => {
          switch ( reason.code) {
            case 'auth/user-not-found':
              this.loginError = 'Nous n\'avons pas trouver ton email.';
              break;
            case 'auth/wrong-password':
              this.loginError = 'Ton mot de passe n\'est pas bon.';
              this.showPassworForgotten = true;
              break;
            case 'auth/user-disabled':
              this.loginError = 'Ton compte a été désactivé.';
              break;
            default:
              this.loginError = reason.message;
              break;
          }
        });
  }

  sendRenewPassword(): void {
    const email = this.form.controls['email'].value;
    this.authService.auth.sendPasswordResetEmail(email)
      .then(() => {
        this.emailSent = 'Un mail de reinitialisation de ton passe t\'a été envoyé. Verifie tes mails.';
      })
      .catch(reason => {
        this.loginError = 'Verifie ton adresse email';
      });
  }

  private createForm() {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }
}
