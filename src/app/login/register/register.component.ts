import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidationErrors, AbstractControl } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'sg-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: FormGroup;
  registerError: string;
  validationMessage: string;

  constructor(private fb: FormBuilder,
    private authService: AngularFireAuth) { }

  ngOnInit() {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email] ],
      password: ['', [Validators.required] ],
      confirmPassword: ['', [Validators.required, this.checkPassword] ]
    });
  }

  register(): void {
    const controls = this.form.controls;
    const auth = this.authService.auth;
    auth.createUserWithEmailAndPassword(controls['email'].value, controls['password'].value)
      .then(response => {
        const user = response.user;
        auth.updateCurrentUser(user)
          .then(() => {
            user.sendEmailVerification()
              .then(() => this.validationMessage = 'Un mail de confirmation t\'a été envoyé. Vérifie ta boite mail et clique sur le lien.')
              .catch(reason => this.registerError = reason.message);
          })
          .catch(reason => this.registerError = reason.message);
      })
      .catch(reason => {
        switch (reason.code) {
          case 'auth/email-already-in-use':
            this.registerError = 'Cet email existe déja.';
            break;
          default:
            this.registerError = reason.message;
            break;
        }
      });
  }

  getEmailError(error: ValidationErrors) {
    if (!error) {
      return;
    }
    if (error['required']) {
      return 'ton adresse email est requise';
    }
    if (error['email']) {
      return 'ton adresse email n\'est pas valide';
    }
  }

  getConfirmPasswordError(error: ValidationErrors) {
    if (!error) {
      return;
    }
    if (error['required']) {
      return 'Ce champ est requis';
    }
    if (error['checkPassword']) {
      return 'Les mots de passe ne corresponde pas';
    }
  }

  private checkPassword(control: AbstractControl): ValidationErrors | null {
    if (!control || !control.parent || !control.parent.controls) {
      return;
    }
    const controls = control.parent.controls;
    if (controls['password'].value !== controls['confirmPassword'].value) {
      return {
        'checkPassword': 'Ton mot de passe n\'est pas confirmer'
      };
    }
  }
}
