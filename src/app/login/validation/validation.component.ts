import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Validators as validators } from '../validators';

@Component({
  selector: 'sg-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.css']
})
export class ValidationComponent implements OnInit {
  registerError: string;
  validationMessage: string;
  mode: string;
  form: FormGroup;
  oobCode: string;
  reseted = false;

  constructor(private authService: AngularFireAuth,
    private fb: FormBuilder) { }

  ngOnInit() {
    if (!location) {
      return;
    }

    const parameters = location.search.substring(1).split('&');
    parameters.forEach(p => {
      const segments = p.split('=');
      const key = segments[0];
      const value = segments[1];
      switch (key) {
        case 'oobCode':
        this.oobCode = value;
        break;
        case 'mode':
        this.mode = value;
        break;
      }
    });
    if (this.mode === 'verifyEmail') {
      const auth = this.authService.auth;
      auth.applyActionCode(this.oobCode)
        .catch(() => {
          this.registerError = 'Ah, il y eu problème';
        });
    } else {
      this.form = this.fb.group({
        password: ['', [Validators.required] ],
        confirmPassword: ['', [Validators.required, validators.checkPassword] ]
      });
    }
  }

  sendEmail(): void {
    this.authService.auth.currentUser
      .sendEmailVerification()
      .then(() => {
        this.validationMessage = 'Un mail de confirmation t\'a été envoyé. Vérifie ta boite mail et clique sur le lien.';
      })
      .catch(reason => this.registerError = reason.message);
  }

  reset(): void {
    this.authService.auth.confirmPasswordReset(this.oobCode, this.form.controls['password'].value)
      .then(() => this.reseted = true)
      .catch(reason => {
        this.registerError = 'Hum ! ça marche pas.';
      });
  }
}
