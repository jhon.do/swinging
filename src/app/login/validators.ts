import { AbstractControl, ValidationErrors } from '@angular/forms';
export class Validators {
  public static checkPassword(control: AbstractControl): ValidationErrors | null {
    if (!control || !control.parent || !control.parent.controls) {
      return;
    }
    const controls = control.parent.controls;
    if (controls['password'].value !== controls['confirmPassword'].value) {
      console.error('password check');
      return {
        'checkPassword': 'Ton mot de passe n\'est pas confirmer'
      };
    }
  }
}
