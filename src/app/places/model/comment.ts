/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
export interface Comment {
  placeId: string;
  fromId: string;
  rating: number;
  text: string;
}
