export enum ContactType {
  'url' = 'Site web',
  'email' = 'Adresse mail',
  'phone' = 'Numéro de téléphone'
}
