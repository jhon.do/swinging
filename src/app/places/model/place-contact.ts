/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { ContactType } from './contact-type.enum';

export interface PlaceContact {
  type: ContactType;
  value: string;
}
