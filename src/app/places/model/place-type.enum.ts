/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
export enum PlaceType {
  'club' = 'un club sans balnéo',
  'sauna' = 'un sauna sans disco',
  'sauna-club' = 'un club avec balnéo',
  'plage' = 'un coin naturiste',
  'exterieur' = 'un extérieur',
  'lingerie' = 'une boutique de lingerie',
  'sexshop' = 'un sex-shop',
  'organizer' = 'organisateur de soirée',
  'hote' = 'une maison d\'hôtes'
}
