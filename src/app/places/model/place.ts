import { PlaceContact } from './place-contact';
import { firestore } from 'firebase';
/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { PlaceType } from './place-type.enum';

export interface Place {
  fromId: string;
  type: PlaceType;
  name: string;
  description: string;
  votes?: number;
  cleanliness?: number;
  vibes?: number;
  contacts: PlaceContact[];
}
