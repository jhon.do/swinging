/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormBuilder, FormArray } from '@angular/forms';
import { Subscription } from 'rxjs';
import { GeoFirestore } from 'geofirestore';

import { PlaceType } from '../model/place-type.enum';
import { ContactType } from '../model/contact-type.enum';
import { Place } from '../model/place';

@Component({
  selector: 'sg-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit, OnDestroy {
  @ViewChild('autocomplete')
  inputRef: ElementRef;
  map: google.maps.Map;
  form: FormGroup;
  placeTypes = Object.keys(PlaceType).map(key => ({ key: key, value: PlaceType[key]}));
  contactTypes = Object.keys(ContactType).map(key => ({ key: key, value: ContactType[key]}));
  error: string;

  private uid: string;
  private userSubscription: Subscription;
  private marker: google.maps.Marker;
  private location: google.maps.LatLng;
  private placeId: string;

  constructor(private fb: FormBuilder,
    private authService: AngularFireAuth,
    private db: AngularFirestore) { }

  ngOnInit() {
    this.userSubscription = this.authService.user.subscribe(user => {
      if (!user) {
        return;
      }
      this.uid = user.uid;
      this.form = this.fb.group({
        name: [ '', Validators.required ],
        placeType: ['', Validators.required ],
        photoUrl: [''],
        location: ['', Validators.required ],
        description: [''],
        contacts: this.fb.array([])
      });
    });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  mapReady(m: google.maps.Map): void {
    this.map = m;
    m.setZoom(13);
    const input = <HTMLInputElement>this.inputRef.nativeElement;
    const autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.addListener('place_changed', () => {
      const place = autocomplete.getPlace();
      if (place && place.geometry && place.geometry.location) {
        this.placeId = place.place_id;
        this.location = place.geometry.location;
        if (this.marker) {
          this.marker.setMap(null);
        }
        this.marker = new google.maps.Marker({
          position: this.location,
          map: this.map
        });
        this.map.setCenter(this.location);
        const controls = this.form.controls;
        controls['name'].setValue(place.name);
        const contacts = (<FormArray>controls['contacts']);
        if (place.international_phone_number) {
          this.addContactGroup(contacts,
            this.contactTypes.find(t => t.value === ContactType.phone.toString()).key,
            place.international_phone_number);
        }
        if (place.website) {
          this.addContactGroup(contacts,
            this.contactTypes.find(t => t.value === ContactType.url.toString()).key,
            place.website);
        }
        if (place.photos && place.photos.length > 0) {
          controls['photoUrl'].setValue(place.photos[0].getUrl({
            maxHeight: 100,
            maxWidth: 100
          }));
        }
      }
    });
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        const coords = position.coords;
        this.map.setCenter(new google.maps.LatLng(coords.latitude, coords.longitude));
      });
    }
  }

  addContact(): void {
    const array = <FormArray>this.form.controls['contacts'];
    array.push(this.fb.group({
      contactType: ['', Validators.required],
      value: ['', Validators.required]
    }));
  }

  removeContact(index: number): void {
    const array = <FormArray>this.form.controls['contacts'];
    array.removeAt(index);
  }

  getContactPlaceHolder(type: string): string {
    switch (type) {
      case 'url':
        return 'http://';
      case 'email':
        return 'email@site.com';
      case 'phone':
        return '01 02 03 04 05';
    }
  }

  save(): void {
    const collection = this.db.collection<Place>('places');
    const collectionRef = collection.ref;
    const geoFirestore = new GeoFirestore(collectionRef);
    const controls = this.form.controls;
    const contacts = [];
    const contactsControl = (<FormArray>controls['contacts']).controls;
    contactsControl.forEach((g: FormGroup) => {
      contacts.push({ type: g.controls['contactType'].value, value: g.controls['value'].value});
    });
    const place: Place = {
      fromId: this.uid,
      type: controls['placeType'].value,
      name: controls['name'].value,
      description: controls['description'].value,
      contacts: contacts
    };
    geoFirestore.setWithDocument(this.placeId, [this.location.lat(), this.location.lng()], place)
      .catch(error => this.error = error.message);
  }

  private addContactGroup(contacts: FormArray, type: string, value: string): void {
    if (contacts.controls.findIndex(c => c['contactType'] && c['contactType'].value === type) < 0) {
      contacts.push(this.fb.group({
        contactType: [type, Validators.required],
        value: [value, Validators.required]
      }));
    }
  }
}
