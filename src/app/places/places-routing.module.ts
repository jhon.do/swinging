/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewComponent } from './view/view.component';
import { NewComponent } from './new/new.component';

const routes: Routes = [
  {
    path: 'new',
    component: NewComponent
  },
  {
    path: ':id',
    component: ViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlacesRoutingModule { }
