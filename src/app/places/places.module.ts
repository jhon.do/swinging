/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatSelectModule, MatDialogModule } from '@angular/material';
import { PlacesRoutingModule } from './places-routing.module';
import { AgmCoreModule } from '@agm/core';

import { SharedModule } from '../shared/shared.module';
import { NewComponent } from './new/new.component';
import { ViewComponent } from './view/view.component';
import { AddCommentComponent } from './view/add-comment/add-comment.component';

@NgModule({
  imports: [
    CommonModule,
    PlacesRoutingModule,
    SharedModule,
    MatSelectModule,
    AgmCoreModule,
    MatDialogModule,
    FormsModule
  ],
  declarations: [NewComponent, ViewComponent, AddCommentComponent],
  entryComponents: [
    AddCommentComponent
  ]
})
export class PlacesModule { }
