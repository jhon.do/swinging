/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AngularFirestore } from 'angularfire2/firestore';

import { Place } from './../../model/place';

@Component({
  selector: 'sg-add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.css']
})
export class AddCommentComponent implements OnInit {
  data = {
    rating: 0,
    comment: ''
  };

  constructor(
    private dialogRef: MatDialogRef<AddCommentComponent>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

  setRating(rating: number): void {
    this.data.rating = rating;
  }
}
