/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { MatDialog } from '@angular/material';
import { AngularFireAuth } from 'angularfire2/auth';

import { Place } from './../model/place';
import { PlaceType } from './../model/place-type.enum';
import { ContactType } from '../model/contact-type.enum';
import { AddCommentComponent } from './add-comment/add-comment.component';
import { Comment } from './../model/comment';

@Component({
  selector: 'sg-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  loading = true;
  error: string;
  place: Place;
  address: string;
  reviews: google.maps.places.PlaceReview[] = [];
  website: string;
  rating: number;

  private id: string;
  private marker: google.maps.Marker;
  private placeService: google.maps.places.PlacesService;
  private map: google.maps.Map;

  constructor(private route: ActivatedRoute,
    private authService: AngularFireAuth,
    private db: AngularFirestore,
    private dialog: MatDialog) { }

    ngOnInit() {
      this.route.paramMap.subscribe((params: ParamMap) => {
        this.id = params.get('id');
        if (!this.id) {
          this.onError('ça n\'existe pas');
        }
        this.db.collection('places')
          .doc(this.id).ref.get()
          .then(doc => {
            if (!doc) {
              this.onError('ça n\'existe pas');
            }
            const data = doc.data();
            if (!data || !data.d) {
              this.onError('ça n\'existe pas');
            }
            this.place = <Place>data.d;
            const website = this.place.contacts.find(c => c.type === ContactType.url);
            if (website) {
              this.website = website.value;
            }
            this.getDetails();
            this.db.collection('comments', ref => ref.where('placeId', '==', this.id))
              .ref
              .get()
              .then(snapshot => {
                snapshot.docs.forEach(document => {
                  const comment = <Comment>document.data();
                  if (comment && comment.placeId === this.id) {
                    this.db.collection('users').doc(comment.fromId).ref.get()
                    .then(profil => {
                      this.addReview(comment.rating, profil.get('displayName'), comment.text);
                    });
                  }
                });
              });
            this.loading = false;
          });
      });
    }

  mapReady(map: google.maps.Map): void {
    this.map = map;
    map.setZoom(13);
    this.placeService = new google.maps.places.PlacesService(map);
    this.getDetails();
  }

  getType(type: PlaceType): string {
    return PlaceType[type];
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddCommentComponent);

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          this.authService.user.subscribe(user => {
            if (!user) {
              return;
            }
            const commentsRef = this.db.collection('comments').ref;
            commentsRef.add({
              fromId: user.uid,
              placeId: this.id,
              rating: result.rating,
              text: result.comment
            })
            .then(() => {
              this.addReview(result.rating, user.displayName, result.comment);
            })
            .catch(raison => console.error(raison));
        });
        }
      });
  }

  private addReview(rating: number, from: string, text: string): void {
    const review = {
      aspects: [],
      rating: rating,
      author_name: from,
      author_url: `chat/${from}`,
      language: 'fr',
      text: text
    };
    this.reviews.push(review);
  }

  private getDetails() {
    if (!this.map || !this.place) {
      return;
    }
    this.placeService.getDetails({
      placeId: this.id,
      fields: ['formatted_address', 'rating', 'geometry', 'name', 'review', 'opening_hours', 'website']
    }, result => {
      this.address = result.formatted_address;
      const postion = result.geometry.location;
      this.map.setCenter(postion);
      if (this.marker) {
        this.marker.setMap(null);
      }
      this.marker = new google.maps.Marker({
        position: postion,
        map: this.map
      });

      this.reviews = result.reviews || [];
      if (result.website) {
        this.website = result.website;
      }
      this.rating = Math.round(result.rating);
    });
  }

  private onError(error: string): void {
    this.error = error;
    this.loading = false;
  }
}
