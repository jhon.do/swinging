/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { TestBed, inject } from '@angular/core/testing';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';

import { LoggedGuardService } from './logged-guard.service';

describe('LoggedGuardService', () => {
  let authService: AngularFireAuth;
  let router: jasmine.SpyObj<Router>;
  beforeEach(() => {
    const auth = {
      auth: {
        currentUser: undefined
      }
    } as AngularFireAuth;
    const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
    TestBed.configureTestingModule({
      providers: [
        LoggedGuardService,
        { provide: AngularFireAuth, useValue: auth },
        { provide: Router, useValue: routerSpy }
      ]
    });
    router = TestBed.get(Router);
    authService = TestBed.get(AngularFireAuth);
  });

  it('should be created', inject([LoggedGuardService], (service: LoggedGuardService) => {
    expect(service).toBeTruthy();
  }));

  it('canActivate should to be falsy', inject([LoggedGuardService], (service: LoggedGuardService) => {
    expect(service.canActivate()).toBeFalsy();
  }));

  describe('user logger', () => {
    beforeEach(() => {
      authService.auth.currentUser = {
        isAnonymous: false
      } as firebase.User;
    });

    it('canActivate should to be truthy', inject([LoggedGuardService], (service: LoggedGuardService) => {
      expect(service.canActivate()).toBeTruthy();
    }));

    it('canActivate should navigate to home', inject([LoggedGuardService], (service: LoggedGuardService) => {
      router.navigate.and.returnValue(true);
      service.canActivate();
      expect(router.navigate.calls.count()).toBe(1);
    }));

    describe('as anonymous', () => {
      beforeEach(() => {
        authService.auth.currentUser = {
          isAnonymous: true
        } as firebase.User;
        });

      it('canActivate should to be falsy', inject([LoggedGuardService], (service: LoggedGuardService) => {
        expect(service.canActivate()).toBeFalsy();
      }));
    });
  });
});
