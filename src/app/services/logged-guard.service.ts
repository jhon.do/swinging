/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class LoggedGuardService implements CanActivate {

  canActivate(): Observable<boolean> {
    return this.authService.user.pipe(map(user => {
      const result = user && !user.isAnonymous && user.emailVerified;
      if (!result) {
        this.router.navigate(['login']);
      }
      return result;
    }));
  }

  constructor(private authService: AngularFireAuth, private router: Router) { }
}
