/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { Router, CanActivate } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProfileGuardService implements CanActivate {

  canActivate(): Observable<boolean> {
    return this.authService.user.pipe(map(user => {
      if (user && !user.displayName) {
        this.router.navigate(['profile']);
        return false;
      }
      return true;
    }));
  }


  constructor(private authService: AngularFireAuth, private router: Router) { }
}
