/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { ProfilType } from './profil-type.enum';
import { SubProfil } from './sub-profil';

export interface Profil {
  displayName: string;
  photoUrl?: string;
  profilType: ProfilType;
  subProfil1: SubProfil;
  subProfil2?: SubProfil;
  timestamp: any;
}
