/*
 * Project: swinging
 * Copyright 2018 @swinging
 */
import { Subscription } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';
import {
  FormBuilder,
  Validators,
  FormGroup,
  ValidatorFn,
  AsyncValidatorFn,
  ValidationErrors,
  AbstractControl
} from '@angular/forms';
import { firestore } from 'firebase';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';

import { Profil } from '../model/profil';
import { SubProfil } from '../model/sub-profil';
import { ProfilType } from '../model/profil-type.enum';

@Component({
  selector: 'sg-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {
  form: FormGroup;
  error: string;
  displayName: string;
  uid: string;
  profilTypes = Object.keys(ProfilType).map(key => ({ key: key, value: ProfilType[key]}));

  private userSubscription: Subscription;
  private dbSubscription: Subscription;
  constructor(private fb: FormBuilder,
    private authService: AngularFireAuth,
    private db: AngularFirestore) { }

  ngOnInit() {
    this.userSubscription = this.authService.user.subscribe(user => {
      if (!user) {
        return;
      }
      this.uid = user.uid;
      this.form = this.fb.group({
        displayName: [user.displayName, [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(20),
          this.invalidDisplayName()
        ], this.displayNameAlreadyExists()],
        profilType: ['', Validators.required],
        photoUrl: [''],
        subProfil1: this.getSubProfilGroup(),
        subProfil2: this.getSubProfilGroup()
      });
      const controls = this.form.controls;
      this.dbSubscription = this.db.collection<Profil>('users')
        .doc<Profil>(this.uid)
        .valueChanges()
        .subscribe(profil => {
          if (profil) {
            this.displayName = profil.displayName;
            controls['displayName'].disable();
            controls['displayName'].setValue(this.displayName);
            controls['profilType'].disable();
            controls['photoUrl'].setValue(profil.photoUrl);
            controls['profilType'].setValue(profil.profilType);
            this.setSubProfil(controls['subProfil1'] as FormGroup, profil.subProfil1);
            this.setSubProfil(controls['subProfil2'] as FormGroup, profil.subProfil2);
            const subProfilGroup1 = controls['subProfil1'] as FormGroup;
            const subProfilGroup2 = profil.profilType === ProfilType.c ? controls['subProfil2'] as FormGroup : undefined;

            if (this.maxBirthDate(subProfilGroup1.controls['birthDate']) ||
              (subProfilGroup2 && this.maxBirthDate(subProfilGroup2.controls['birthDate']))) {
              this.error = 'Tu n\'est pas encore majeur. Reviens nous voir quand ce sera le cas';
            } else if (!user.displayName) {
              user.updateProfile({
                displayName: profil.displayName,
                photoURL: undefined
              })
              .catch(() => this.error = 'Aye, ça marche pas.');
            }
          }
        });
    });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
    if (this.dbSubscription) {
      this.dbSubscription.unsubscribe();
    }
  }

  save(): void {
    const auth = this.authService.auth;
    const user = auth.currentUser;
    const controls = this.form.controls;
    const displayName = controls['displayName'].value;
    const profilType = controls['profilType'].value;
    const subProfilGroup1 = controls['subProfil1'] as FormGroup;
    const subProfil1 = this.getSubProfil(subProfilGroup1);
    const subProfilGroup2 = profilType === 'c' ? controls['subProfil2'] as FormGroup : this.getSubProfilGroup();
    const subProfil2 = subProfilGroup2 ? this.getSubProfil(subProfilGroup2) : null;
    this.db.collection('users')
      .doc<Profil>(this.uid)
      .set({
        displayName: displayName,
        photoUrl: controls['photoUrl'].value,
        profilType: controls['profilType'].value,
        subProfil1: subProfil1,
        subProfil2: subProfil2,
        timestamp: firestore.FieldValue.serverTimestamp()
      })
      .then(() => {
        if (this.maxBirthDate(subProfilGroup1.controls['birthDate']) || this.maxBirthDate(subProfilGroup2.controls['birthDate'])) {
          this.displayName = displayName;
          this.error = 'Tu n\'est pas encore majeur. Reviens nous voir quand ce sera le cas';
        } else {
          delete this.error;
          user.updateProfile({
            displayName: displayName,
            photoURL: undefined
          })
          .catch(() => this.error = 'Aye, ça marche pas.');
        }
      })
      .catch(() => 'Aye, ça marche pas.');
  }

  getGenre(key: ProfilType): string {
    return ProfilType[key];
  }

  profilTypeChange(change: string): void {
    const group = <FormGroup>this.form.controls['subProfil2'];
    if (change !== 'c') {
      group.controls['birthDate'].disable();
    } else {
      group.controls['birthDate'].enable();
    }
  }

  getErrorDisplayNameError(error: ValidationErrors): string {
    if (!error) {
      return;
    }
    if (error['minlength']) {
      return 'Ton pseudo doit contenir au moins 3 caractères';
    }
    if (error['maxlength']) {
      return 'Ton pseudo ne pas faire plus de 20 caractères';
    }
    if (error['displayNameAlreadyExists']) {
      return 'Ce pseudo existe déjà';
    }
    if (error['required']) {
      return 'Le pseudo est requis';
    }
    if (error['invalidDisplayName']) {
      return 'Ce pseudo n\'est pas autorisé';
    }
  }

  private setSubProfil(group: FormGroup, profil: SubProfil): void {
    if (profil) {
      const controls = group.controls;
      const birthDate = controls['birthDate'];
      birthDate.setValue(new Date(profil.birthDate));
      birthDate.disable();
    }
  }

  private getSubProfil(c: FormGroup): SubProfil {
    const controls = c.controls;
    const birthDate = <Date>controls['birthDate'].value;
    return {
      birthDate: birthDate ? birthDate.toISOString() : null
    };
  }

  private getSubProfilGroup(): FormGroup {
    return this.fb.group({
      birthDate: [undefined, [Validators.required]]
    });
  }

  private invalidDisplayName(): ValidatorFn {
    const fn = Validators.pattern('__.*__');
    return c => {
      if (!c.value) {
        return;
      }
      if (!fn(c)) {
        return { invalidDisplayName: '__.*__' };
      }
    };
  }

  private displayNameAlreadyExists(): AsyncValidatorFn {
    const db = this.db;
    return c => {
      return new Promise(resolve => {
        if (!c.value) {
          resolve();
        }

        db.collection<Profil>('users', ref => ref.where('displayName', '==', c.value))
          .valueChanges()
          .subscribe(profils => {
            if (profils.length === 0) {
              resolve();
            }
            resolve({ displayNameAlreadyExists: true });
          });
        });
    };
  }

  private maxBirthDate(c: AbstractControl): ValidationErrors | null {
    const now = new Date();
    const year = now.getFullYear() - 18;
    const maxDate = new Date(year, now.getMonth(), now.getDate());
    if (c.value && c.value instanceof Date && c.value > maxDate) {
      return { maxBirthDate: true };
    }
  }
}
