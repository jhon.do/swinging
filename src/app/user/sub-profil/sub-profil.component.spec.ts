import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubProfilComponent } from './sub-profil.component';

describe('SubProfilComponent', () => {
  let component: SubProfilComponent;
  let fixture: ComponentFixture<SubProfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubProfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubProfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
