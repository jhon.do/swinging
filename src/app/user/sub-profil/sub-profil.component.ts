import { FormGroup } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'sg-sub-profil',
  templateUrl: './sub-profil.component.html',
  styleUrls: ['./sub-profil.component.css']
})
export class SubProfilComponent implements OnInit {
  @Input()
  form: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
